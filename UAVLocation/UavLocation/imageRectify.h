#ifndef __IMAGE_RECTIFY__
#define __IMAGE_RECTIFY__

//相机参数：焦距、主点
#define FLENX 1500.4252f
#define FLENY 1500.5009f
#define PX 1027.4212f
#define PY 1065.9370f

//相机参数：5参畸变模型
#define D1 -0.17087679717845841
#define D2 0.11691290117940221
#define D3 -0.00029030571792900
#define D4 0.000440613299114128
#define D5 -0.02135738671697512

//功能：图像畸变校正，根据相机参数、畸变模型参数对图像进行校正
//输入：原图像（OrgRealImg）
//输出：返回畸变校正完成图像
Mat remapTrim(Mat OrgRealImg);

//功能：根据三轴姿态校正图像。
//输入：三轴转动量（rx,ry,rz）弧度
//输出/返回：图像姿态变换矩阵
Mat RotationHomographyEstimation(double rx,double ry, double rz);

//根据高度估计图像尺寸调整比例
//输入：相对地面高度（altitude），单位米
//输出：图像尺寸调整比例
double zoom_etimation(double altitude);

//根据图像姿态变换矩阵、图像尺寸、图像尺寸调整比例，计算变换矩阵
//输入：原图像尺寸（imgSize），图像姿态变换矩阵（H），图像尺寸调整比例（scale_ratio）
//输出/返回：调整后图像尺寸（imgSize），返回图像变换关系
Mat H_re_estimation(Size &imgSize,Mat H,double scale_ratio);
#endif