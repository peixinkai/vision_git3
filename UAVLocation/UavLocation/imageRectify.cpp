#include "stdafx.h"
#include "common.h"
#include "imageRectify.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>


//功能：图像畸变校正，根据相机参数、畸变模型参数对图像进行校正
//输入：原图像（OrgRealImg）
//输出：返回畸变校正完成图像
Mat remapTrim(Mat OrgRealImg)
{
	Mat cameraMatrix = (Mat_<float>(3,3) << FLENX, 0.0f, PX, 0.0f, FLENY, PY, 0.0f, 0.0f, 1.0f);
	Mat distCoeffs = (Mat_<double>(5,1) << D1,D2,D3,D4,D5);
	//Mat distCoeffs = (Mat_<double>(5,1) << -1.1520801634361276e-001,7.0341922597679421e-002,0.0, 0.0, -1.9746531553599293e-002);
	Mat temp = OrgRealImg.clone();
	temp.setTo(0);
	//undistort(OrgRealImg, temp, cameraMatrix, distCoeffs);
	Size imageSize = OrgRealImg.size();
	Mat map1,map2;
	initUndistortRectifyMap(
		cameraMatrix, distCoeffs, Mat(),
		getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0), imageSize,
		CV_16SC2, map1, map2);
	remap(OrgRealImg, temp, map1, map2, INTER_LINEAR);
	int dstw = OrgRealImg.cols;
	int dsth = OrgRealImg.rows;
	Rect roi = Rect(0,0,dstw,dsth);
	Mat trimImg = temp(roi);
	return trimImg;
}


Mat HomographyEstimation(Size &imgSize, Point2f topLeft,Point2f  topRight,\
	Point2f bottomLeft,Point2f  bottomRight,float compress_ratio)
{
	float minx = min(topLeft.x,min(topRight.x,min(bottomLeft.x,bottomRight.x)));
	float miny = min(topLeft.y,min(topRight.y,min(bottomLeft.y,bottomRight.y)));
	float maxx = max(topLeft.x,max(topRight.x,max(bottomLeft.x,bottomRight.x)));
	float maxy = max(topLeft.y,max(topRight.y,max(bottomLeft.y,bottomRight.y)));
	std::vector<cv::Point2f> srcps, dstps;
	for (int i = 0; i < 2; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			cv::Point2f sp,dp;
			sp.x = float(j * imgSize.width);
			sp.y = float(i * imgSize.height);
			srcps.push_back(sp);
			switch (i*2+j)
			{
			case 0 : {dp = topLeft; break;}
			case 1 : {dp = topRight; break;}
			case 2 : {dp = bottomLeft; break;}
			case 3 : {dp = bottomRight; break;}
			default: {}
			}
			dp.x -= minx; dp.y -= miny; 
			dp.x = dp.x * compress_ratio;
			dp.y = dp.y * compress_ratio;
			dstps.push_back(dp);
		}
	}
	imgSize.width = cvRound((maxx - minx) * compress_ratio); //cvRound四舍五入取整
	imgSize.height = cvRound((maxy - miny) * compress_ratio);
	return getPerspectiveTransform(srcps,dstps);//在这里，透视变换计算的矩阵是不是除了姿态变换，还将实时图分辨率变成1m？
}

//功能：根据三轴姿态校正图像。
//输入：三轴转动量（rx,ry,rz）弧度
//输出/返回：图像姿态变换矩阵
Mat RotationHomographyEstimation(double rx,double ry, double rz)
{
	rx = Deg2Rad(rx); ry = Deg2Rad(ry); rz = Deg2Rad(rz);
	Mat K = (Mat_<double>(3,3) << FLENX, 0.0, PX , 0.0, FLENY, PY , 0.0, 0.0, 1.0);
	Mat Rx = (Mat_<double>(3,3) << 1.0, 0.0, 0.0, 0.0, cos(rx),-sin(rx), \
		0.0,sin(rx), cos(rx));
	Mat Ry = (Mat_<double>(3,3) << cos(ry), 0.0, sin(ry), 0.0,1.0,0.0, \
		-sin(ry),0.0, cos(ry));
	Mat Rz = (Mat_<double>(3,3) << cos(rz), -sin(rz), 0.0,sin(rz),\
		cos(rz), 0.0,0.0,0.0, 1.0);
	Mat HR = K * Rx * Ry * Rz * K.inv();
	return HR;
}


//根据高度估计图像尺寸调整比例
//输入：相对地面高度（altitude），单位米
//输出：图像尺寸调整比例
double zoom_etimation(double altitude)
{
	double Flenth = (FLENX + FLENY) / 2.0;
	return altitude / Flenth;
}

//根据图像姿态变换矩阵、图像尺寸、图像尺寸调整比例，计算变换矩阵
//输入：原图像尺寸（imgSize），图像姿态变换矩阵（H），图像尺寸调整比例（scale_ratio）
//输出/返回：调整后图像尺寸（imgSize），返回图像变换关系
Mat H_re_estimation(Size &imgSize,Mat H,double compress_ratio)
{
	std::vector<cv::Point2f> srcps, dstps;
	for (int i = 0; i < 2; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			cv::Point2f sp,dp;
			sp.x = float(j * imgSize.width);
			sp.y = float(i * imgSize.height);
			srcps.push_back(sp);
		}
	}
	std::cout<<H<<std::endl;  
	perspectiveTransform(srcps,dstps,H);//先通过透视变换得到四个点坐标
	return HomographyEstimation(imgSize,dstps[0],dstps[1],dstps[2],dstps[3],(float) compress_ratio);
}